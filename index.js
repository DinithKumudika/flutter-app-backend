require("dotenv").config();
const express = require("express");
const {initDB} = require('./database/mongoDB');

const port = process.env.PORT || 8999;

app = express();
app.use(express.json());

const ismRouter = require('./controllers/ismDataController');

initDB();

app.use('/ism', ismRouter);

app.listen(port, () => {
    console.log(`App started on port :  ${port}`);
});

