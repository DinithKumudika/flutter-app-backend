const mongoose = require('mongoose');
const {Schema} = mongoose;

const mongoUrl = process.env.MONGO_DB_URL;

const initDB = () => {
    mongoose.connect(mongoUrl, {useNewUrlParser: true});
};


module.exports = {
    initDB
};

