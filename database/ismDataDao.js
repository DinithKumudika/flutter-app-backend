const ISMUserModel = require('./models/ism.user.data.model');

const insertISMUserData = async (data) => {

    const {
        firstName,
        lastName,
        useInternet,
        agetRange,
        browseTime,
        dailyAverageDataUsageMB,
        internetUsageReason,
        internetAccessMethod,
        isp,
        monthlyUsage,
        locationLat,
        locationLng,
    } = data;

    let ismData = new ISMUserModel();

    const uniqueName = firstName.toUpperCase() + '_' + lastName.toUpperCase();

    ismData.uniqueName = uniqueName;
    ismData.firstName = firstName;
    ismData.useInternet = useInternet;
    ismData.agetRange = agetRange;
    ismData.browseTime = browseTime;
    ismData.dailyAverageDataUsageMB = dailyAverageDataUsageMB;
    ismData.internetUsageReason = internetUsageReason;
    ismData.internetAccessMethod = internetAccessMethod;
    ismData.isp = isp;
    ismData.monthlyUsage = monthlyUsage;
    ismData.locationLat = locationLat;
    ismData.locationLng = locationLng;

    await ismData.save();
};

const getISMUserDataByUserName = async (firstName, lastName) => {

    let uniqueName = firstName.toUpperCase() + '_' + lastName.toUpperCase();

    let result = await ISMUserModel.find({
        uniqueName: uniqueName,
    })
        .limit(1)
        .exec();

    return result;
};

const saveUpdateISMData =  async (ismData) => {
    await ismData.save();
};


module.exports = {
    insertISMUserData,
    getISMUserDataByUserName,
    saveUpdateISMData,
};
