const mongoose = require('mongoose');
const {Schema} = mongoose;

const ISMUserData = new Schema({
    uniqueName : String,
    firstName: String,
    lastName: String,
    useInternet: {type: Boolean, default: false},
    agetRange : String,
    browseTime : String,
    dailyAverageDataUsageMB : String,
    internetUsageReason : String,
    internetAccessMethod : String,
    isp : String,
    monthlyUsage : String,
    locationLat : String,
    locationLng : String,
    createdDate: {type: Date, default: Date.now()},
});

module.exports = mongoose.model('ISMUserData', ISMUserData);
