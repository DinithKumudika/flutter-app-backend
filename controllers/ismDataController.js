const express = require("express");
const router = express.Router();
const _ = require('lodash');

const {
    saveUpdateISMUSerData
} = require("../services/ismService");

router.post("/save-update-user-input", async (req, res) => {

    const data = req.body;

    let status = 'FAILED - Invalid Request';

    if(!_.isEmpty(data)) {
        await saveUpdateISMUSerData(data);
        status = 'Success';
    }

    res.send(status);
});

module.exports = router;
