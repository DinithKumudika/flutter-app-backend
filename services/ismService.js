const {
    insertISMUserData,
    getISMUserDataByUserName,
    saveUpdateISMData,
} = require('../database/ismDataDao');

const saveUpdateISMUSerData = async (data = {}) => {

    const uniqueName = data.firstName.toUpperCase() + '_' + data.lastName.toUpperCase();

    console.log('START : Save update ISM user data : ', uniqueName);

    let savedISMData =  await getISMUserDataByUserName(data.firstName, data.lastName);

    console.log(savedISMData);

    if(savedISMData.length ===0) {
        await insertISMUserData(data);
    } else {
        //TODO
        console.log('Update here');
    }

    console.log('END : Save update ISM user data : ', uniqueName);
};


module.exports = {
    saveUpdateISMUSerData,
};